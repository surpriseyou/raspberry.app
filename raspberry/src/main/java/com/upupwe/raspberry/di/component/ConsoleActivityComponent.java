package com.upupwe.raspberry.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.upupwe.raspberry.di.module.ConsoleActivityModule;
import com.upupwe.raspberry.mvp.contract.ConsoleActivityContract;

import com.jess.arms.di.scope.ActivityScope;
import com.upupwe.raspberry.mvp.ui.activity.ConsoleActivity;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 07/01/2019 12:17
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@ActivityScope
@Component(modules = ConsoleActivityModule.class, dependencies = AppComponent.class)
public interface ConsoleActivityComponent {
    void inject(ConsoleActivity activity);

    @Component.Builder
    interface Builder {
        @BindsInstance
        ConsoleActivityComponent.Builder view(ConsoleActivityContract.View view);

        ConsoleActivityComponent.Builder appComponent(AppComponent appComponent);

        ConsoleActivityComponent build();
    }
}
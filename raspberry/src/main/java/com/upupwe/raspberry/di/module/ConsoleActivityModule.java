package com.upupwe.raspberry.di.module;

import dagger.Binds;
import dagger.Module;

import com.upupwe.raspberry.mvp.contract.ConsoleActivityContract;
import com.upupwe.raspberry.mvp.model.ConsoleModel;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 07/01/2019 12:17
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@Module
public abstract class ConsoleActivityModule {

    @Binds
    abstract ConsoleActivityContract.Model bindConsoleActivityModel(ConsoleModel model);
}
package com.upupwe.raspberry.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.upupwe.raspberry.di.module.OperationModule;
import com.upupwe.raspberry.mvp.contract.OperationContract;

import com.jess.arms.di.scope.FragmentScope;
import com.upupwe.raspberry.mvp.ui.fragment.OperationFragment;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 06/26/2019 20:28
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@FragmentScope
@Component(modules = OperationModule.class, dependencies = AppComponent.class)
public interface OperationComponent {
    void inject(OperationFragment fragment);

    @Component.Builder
    interface Builder {
        @BindsInstance
        OperationComponent.Builder view(OperationContract.View view);

        OperationComponent.Builder appComponent(AppComponent appComponent);

        OperationComponent build();
    }
}
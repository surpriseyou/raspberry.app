package com.upupwe.raspberry.di.component;

import dagger.BindsInstance;
import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.upupwe.raspberry.di.module.WelcomeModule;
import com.upupwe.raspberry.mvp.contract.WelcomeContract;

import com.jess.arms.di.scope.ActivityScope;
import com.upupwe.raspberry.mvp.ui.activity.WelcomeActivity;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 06/25/2019 20:37
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@ActivityScope
@Component(modules = WelcomeModule.class, dependencies = AppComponent.class)
public interface WelcomeComponent {
    void inject(WelcomeActivity activity);

    @Component.Builder
    interface Builder {
        @BindsInstance
        WelcomeComponent.Builder view(WelcomeContract.View view);

        WelcomeComponent.Builder appComponent(AppComponent appComponent);

        WelcomeComponent build();
    }
}
package com.upupwe.raspberry.mvp.model.api.service;

import com.upupwe.raspberry.mvp.model.entity.Command;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface CommandService {

    @GET
    Observable<Command> getCommand();

    @POST
    Observable<Command> execCommand();
}

package com.upupwe.raspberry.mvp.model.api.service;

import com.upupwe.raspberry.mvp.model.entity.Version;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface VersionService {
    @GET("/Home/GetVersion")
    Observable<Version> getVersion(@Query("versionCode") Integer versionCode);
}

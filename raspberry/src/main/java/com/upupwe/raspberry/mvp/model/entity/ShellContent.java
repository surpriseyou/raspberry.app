package com.upupwe.raspberry.mvp.model.entity;

public class ShellContent {

    private String content;
    private String datetime;

    public ShellContent() {
    }

    public ShellContent(String content, String datetime) {
        this.content = content;
        this.datetime = datetime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}

package com.upupwe.raspberry.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;

import com.blankj.utilcode.util.TimeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;

import com.upupwe.raspberry.di.component.DaggerConsoleActivityComponent;
import com.upupwe.raspberry.mvp.adapter.ShellContentAdapter;
import com.upupwe.raspberry.mvp.contract.ConsoleActivityContract;
import com.upupwe.raspberry.mvp.model.entity.ShellContent;
import com.upupwe.raspberry.mvp.presenter.ConsolePresenter;

import com.upupwe.raspberry.R;


import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;

import static com.jess.arms.utils.Preconditions.checkNotNull;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 07/01/2019 12:17
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
public class ConsoleActivity extends BaseActivity<ConsolePresenter> implements ConsoleActivityContract.View {


    @BindView(R.id.shell_list_view)
    LRecyclerView mLRecycleView;

    LRecyclerViewAdapter mLRecycleViewAdapter;

    ShellContentAdapter mShellContentAdapter;


    @BindView(R.id.btn_run)
    Button btnRun;
    @BindView(R.id.et_script)
    EditText editScript;
    @BindView(R.id.scroll_view)
    ScrollView scrollView;

    @Override
    public void setupActivityComponent(@NonNull AppComponent appComponent) {
        DaggerConsoleActivityComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .view(this)
                .build()
                .inject(this);
    }

    @Override
    public int initView(@Nullable Bundle savedInstanceState) {
        return R.layout.activity_console; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {
        mShellContentAdapter = new ShellContentAdapter(this);
        mLRecycleViewAdapter = new LRecyclerViewAdapter(mShellContentAdapter);
        //adapter要最先设置
        mLRecycleView.setAdapter(mLRecycleViewAdapter);
        mLRecycleView.setPullRefreshEnabled(false);
        mLRecycleView.setLoadMoreEnabled(false);

        mLRecycleView.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<ShellContent> list = new ArrayList<>();
        list.add(new ShellContent("hello world", "2019-07-01"));
        list.add(new ShellContent("hello .net", "2019-07-01"));
        list.add(new ShellContent("hello .net core", "2019-07-01"));
        list.add(new ShellContent("hello csharp", "2019-07-01"));
        list.add(new ShellContent("hello android", "2019-07-01"));

        mShellContentAdapter.addAll(list);

        btnRun.setOnClickListener(view -> {
            String script = editScript.getText().toString();
            if (TextUtils.isEmpty(script)) {
                ToastUtils.showShort("nothing");
            } else {
                ShellContent shellContent = new ShellContent();
                shellContent.setContent(script);
                String datetime = TimeUtils.date2String(Calendar.getInstance().getTime());
                shellContent.setDatetime(datetime);
                mShellContentAdapter.add(shellContent);
                //清空
//                editScript.setText("");
                editScript.setFocusable(true);
                editScript.setFocusableInTouchMode(true);
                editScript.requestFocus();
                scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scrollView.post(new Runnable() {
                            public void run() {
                                scrollView.fullScroll(View.FOCUS_DOWN);
                            }
                        });
                    }
                });

            }
        });
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.snackbarText(message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }
}

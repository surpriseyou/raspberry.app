package com.upupwe.raspberry.mvp.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.upupwe.raspberry.R;
import com.upupwe.raspberry.mvp.model.entity.ShellContent;
import com.upupwe.raspberry.mvp.viewholder.SuperViewHolder;

public class ShellContentAdapter extends ListBaseAdapter<ShellContent> {

    public ShellContentAdapter(Context context) {
        super(context);
    }

    @Override
    public int getLayoutId() {
        return R.layout.shell_content_list_item;
    }

    @Override
    public void onBindItemHolder(SuperViewHolder holder, int position) {
        ShellContent shellContent = mDataList.get(position);

        TextView timeView = holder.getView(R.id.tv_shell_time);
        TextView contentView = holder.getView(R.id.tv_shell_content);

        timeView.setText(shellContent.getDatetime());
        contentView.setText(shellContent.getContent());
    }
}

package com.upupwe.raspberry.mvp.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.upupwe.raspberry.R;
import com.upupwe.raspberry.mvp.bean.OperationModel;
import com.upupwe.raspberry.mvp.viewholder.SuperViewHolder;

public class OperationAdapter extends ListBaseAdapter<OperationModel> {

    public OperationAdapter(Context context) {
        super(context);
    }

    @Override
    public int getLayoutId() {
        return R.layout.operation_gridview_item;
    }

    @Override
    public void onBindItemHolder(SuperViewHolder holder, int position) {
        OperationModel operationModel = mDataList.get(position);
        TextView textView = holder.getView(R.id.tv_introduction);
        textView.setText(operationModel.info);

        ImageView imageView = holder.getView(R.id.iv_illustration);
        imageView.setImageResource(operationModel.imgRes);
    }
}

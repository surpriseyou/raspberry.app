package com.upupwe.raspberry.mvp.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Version implements Parcelable {
    private int versionCode;
    private String versionMessage;
    private String updateMessage;


    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionMessage() {
        return versionMessage;
    }

    public void setVersionMessage(String versionMessage) {
        this.versionMessage = versionMessage;
    }

    public String getUpdateMessage() {
        return updateMessage;
    }

    public void setUpdateMessage(String updateMessage) {
        this.updateMessage = updateMessage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.versionCode);
        dest.writeString(this.versionMessage);
        dest.writeString(this.updateMessage);
    }

    public Version() {
    }

    protected Version(Parcel in) {
        this.versionCode = in.readInt();
        this.versionMessage = in.readString();
        this.updateMessage = in.readString();
    }

    public static final Parcelable.Creator<Version> CREATOR = new Parcelable.Creator<Version>() {
        @Override
        public Version createFromParcel(Parcel source) {
            return new Version(source);
        }

        @Override
        public Version[] newArray(int size) {
            return new Version[size];
        }
    };
}

package com.upupwe.raspberry.mvp.presenter;

import android.app.Application;
import android.view.View;
import android.widget.Toast;

import com.blankj.utilcode.util.ToastUtils;
import com.jess.arms.integration.AppManager;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.http.imageloader.ImageLoader;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import javax.inject.Inject;

import com.jess.arms.utils.ArmsUtils;
import com.jess.arms.utils.PermissionUtil;
import com.upupwe.raspberry.BuildConfig;
import com.upupwe.raspberry.app.common.BaseDialog;
import com.upupwe.raspberry.app.model.DialogModel;
import com.upupwe.raspberry.app.update.UpdateApkService;
import com.upupwe.raspberry.app.utils.DialogUtils;
import com.upupwe.raspberry.app.utils.RxBus;
import com.upupwe.raspberry.mvp.contract.WelcomeContract;
import com.upupwe.raspberry.mvp.model.entity.Version;

import java.util.List;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 06/25/2019 20:37
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
@ActivityScope
public class WelcomePresenter extends BasePresenter<WelcomeContract.Model, WelcomeContract.View> {
    @Inject
    RxErrorHandler mErrorHandler;
    @Inject
    Application mApplication;
    @Inject
    ImageLoader mImageLoader;
    @Inject
    AppManager mAppManager;

    @Inject
    public WelcomePresenter(WelcomeContract.Model model, WelcomeContract.View rootView) {
        super(model, rootView);
    }


    public void requestPermission() {
        PermissionUtil.externalStorage(new PermissionUtil.RequestPermission() {
            @Override
            public void onRequestPermissionSuccess() {
                UpdateApkService.startUpdateService(mApplication);
            }

            @Override
            public void onRequestPermissionFailure(List<String> permissions) {
                ArmsUtils.snackbarText("请授权！");
                requestPermission();
            }

            @Override
            public void onRequestPermissionFailureWithAskNeverAgain(List<String> permissions) {

            }
        }, mRootView.getRxPermission(), mErrorHandler);
    }

//    public void checkUpdate() {
//        mModel.getVersion()
//                .subscribe(new Observer<Version>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//
//                    }
//
//                    @Override
//                    public void onNext(Version version) {
//                        if (version.getVersionCode() > BuildConfig.VERSION_CODE) {
//                            DialogModel model = new DialogModel("版本更新", version.getVersionMessage());
//                            DialogUtils.ConfirmDialog(model, new BaseDialog.OnConfirmListener() {
//                                @Override
//                                public boolean onCancel(View view) {
//                                    return true;
//                                }
//
//                                @Override
//                                public boolean onConfirm(View view) {
////                                    UpdateApkService.startUpdateService(mApplication);
//                                    return true;
//                                }
//                            });
//                        }
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        ToastUtils.showLong(e.getMessage());
//                    }
//
//                    @Override
//                    public void onComplete() {
//
//                    }
//                });
//    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }
}

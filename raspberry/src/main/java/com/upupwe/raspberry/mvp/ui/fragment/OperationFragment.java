package com.upupwe.raspberry.mvp.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.jdsjlzx.ItemDecoration.GridItemDecoration;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.jess.arms.base.BaseFragment;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.ArmsUtils;
import com.upupwe.raspberry.R;
import com.upupwe.raspberry.di.component.DaggerOperationComponent;
import com.upupwe.raspberry.mvp.adapter.OperationAdapter;
import com.upupwe.raspberry.mvp.bean.OperationModel;
import com.upupwe.raspberry.mvp.contract.OperationContract;
import com.upupwe.raspberry.mvp.presenter.OperationPresenter;
import com.upupwe.raspberry.mvp.ui.activity.ConsoleActivity;

import java.util.Arrays;

import butterknife.BindView;

import static com.jess.arms.utils.Preconditions.checkNotNull;


/**
 * ================================================
 * Description:
 * <p>
 * Created by MVPArmsTemplate on 06/26/2019 20:28
 * <a href="mailto:jess.yan.effort@gmail.com">Contact me</a>
 * <a href="https://github.com/JessYanCoding">Follow me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms">Star me</a>
 * <a href="https://github.com/JessYanCoding/MVPArms/wiki">See me</a>
 * <a href="https://github.com/JessYanCoding/MVPArmsTemplate">模版请保持更新</a>
 * ================================================
 */
public class OperationFragment extends BaseFragment<OperationPresenter> implements OperationContract.View {

    @BindView(R.id.list_view)
    LRecyclerView lRecyclerView;

    LRecyclerViewAdapter mAdapter = null;

    OperationAdapter operationAdapter = null;

    OperationModel[] list = {new OperationModel(1, "", R.drawable.vector_console, "控制台")
            , new OperationModel(2, "", R.drawable.vector_led, "LED")
            , new OperationModel(3, "", R.drawable.vector_temperature, "温度")
            , new OperationModel(4, "", R.drawable.vector_audio, "音频")};


    public static OperationFragment newInstance() {
        OperationFragment fragment = new OperationFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(@NonNull AppComponent appComponent) {
        DaggerOperationComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .view(this)
                .build()
                .inject(this);
    }

    @Override
    public View initView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_operation, container, false);
    }

    @Override
    public void initData(@Nullable Bundle savedInstanceState) {
        operationAdapter = new OperationAdapter(getActivity());
        operationAdapter.addAll(Arrays.asList(list));
        mAdapter = new LRecyclerViewAdapter(operationAdapter);
        GridLayoutManager manager = new GridLayoutManager(getContext(), 2);
        lRecyclerView.setLayoutManager(manager);
        lRecyclerView.setAdapter(mAdapter);
        //根据需要选择使用GridItemDecoration还是SpacesItemDecoration
        GridItemDecoration divider = new GridItemDecoration.Builder(getContext())
                .setColorResource(R.color.divider_line_color)
                .setHorizontal(R.dimen.default_divider_padding)
                .setVertical(R.dimen.default_divider_padding)
                .build();
        lRecyclerView.addItemDecoration(divider);

        lRecyclerView.setLoadMoreEnabled(false);
        lRecyclerView.setPullRefreshEnabled(false);
        mAdapter.setOnItemClickListener((view, position) -> {
            OperationModel operationModel = operationAdapter.getDataList().get(position);
            //ArmsUtils.snackbarText(operationModel.info);
            switch (operationModel.id) {
                case 1:
                    ArmsUtils.startActivity(ConsoleActivity.class);
                    break;
                default:
                    ArmsUtils.snackbarText(operationModel.info);
                    break;
            }
        });


    }

    @Override
    public void setData(@Nullable Object data) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        ArmsUtils.snackbarText(message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        ArmsUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {

    }
}

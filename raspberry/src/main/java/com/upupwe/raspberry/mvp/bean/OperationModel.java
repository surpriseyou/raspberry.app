package com.upupwe.raspberry.mvp.bean;

public class OperationModel {
    public int id;
    public String title;
    public int imgRes;
    public String info;

    public OperationModel(int id, String title, int imgRes, String info) {
        this.id = id;
        this.title = title;
        this.imgRes = imgRes;
        this.info = info;
    }

    public OperationModel() {
    }
}

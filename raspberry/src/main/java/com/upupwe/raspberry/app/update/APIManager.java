package com.upupwe.raspberry.app.update;

import com.upupwe.raspberry.mvp.model.entity.Version;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Streaming;

public interface APIManager {

    String APP_DOMAIN = "http://118.31.127.146/";

    @Streaming
    @GET("/Home/GetApk")
    Observable<ResponseBody> download();

    @GET("/Home/GetVersion")
    Observable<Version> getVersion(@Query("versionCode") Integer versionCode);
}

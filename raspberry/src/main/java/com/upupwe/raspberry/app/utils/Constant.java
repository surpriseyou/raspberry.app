package com.upupwe.raspberry.app.utils;

import android.os.Environment;

public class Constant {

    public final static String APP_ROOT_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" ;

    public final static String DOWNLOAD_DIR = "/downlaod/";
}
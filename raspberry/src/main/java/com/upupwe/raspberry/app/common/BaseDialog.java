package com.upupwe.raspberry.app.common;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jess.arms.utils.ArmsUtils;
import com.upupwe.raspberry.R;


public class BaseDialog extends Dialog {
    public TextView title;
    public Button cancel;
    public Button confirm;
    private Context ct;
    private View view;
    private LinearLayout layout;
    private View content;
    private OnClickListener mClickListener;
    private int mWidth;
    private int mHeight;
    private OnConfirmListener mConfirmListener;

    public BaseDialog(Context ct, View content, String title, int w, int h) {
        super(ct, R.style.base_dialog);
        this.ct = ct;
        this.content = content;
        this.mWidth = w;
        this.mHeight = h;
        this.view = getLayoutInflater().inflate(R.layout.dialog_base, null);
        setContentView(this.view);
        this.layout = this.view.findViewById(R.id.base_dialog_content);
        this.title = this.view.findViewById(R.id.base_dialog_title_text);
        this.cancel = this.view.findViewById(R.id.base_dialog_cancel_btn);
        this.confirm = this.view.findViewById(R.id.base_dialog_confirm_btn);
        if (this.title != null && title != null && !title.equals("")) this.title.setText(title);
        this.layout.addView(this.content);
        Window window = getWindow();
        assert window != null;
        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.CENTER;
        window.setAttributes(params);
        if (w > 0) content.setMinimumWidth(ArmsUtils.dip2px(this.ct, w));
        if (h > 0) content.setMinimumHeight(ArmsUtils.dip2px(this.ct, h));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.title.setLetterSpacing(.5f);
        }
        cancel.setOnClickListener(v -> {
            if (mConfirmListener != null) {
                if (mConfirmListener.onCancel(view)) {
                    this.dismiss();
                    return;
                }
//                AnimationUtils.startShakeByProperty(view, 1F, 1F, 1.5F, 2L);
                return;
            }
            this.dismiss();
        });
        confirm.setOnClickListener(v -> {
            if (mConfirmListener != null) {
                if (mConfirmListener.onConfirm(view)) {
                    this.dismiss();
                    return;
                }
                return;
            }
            if (mClickListener != null) {
                if (mClickListener.onConfirm()) {
                    this.dismiss();
                    return;
                }
                return;
            }
            this.dismiss();
        });
    }

    public void setOnConfirmListener(OnConfirmListener listener) {
        this.mConfirmListener = listener;
    }

    public void setOnShowListener(OnShowListener listener) {
        super.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                if (listener != null) listener.onShow(getWindow(), view);
            }
        });
    }

    public void setOnClickListener(OnClickListener listener) {
        this.mClickListener = listener;
    }

    public void show() {
        super.show();
        if (mWidth < 0 || mHeight < 0) {
            getWindow().setLayout(mWidth, mHeight);
            LinearLayout ll = (LinearLayout) view;
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) ll.getLayoutParams();
            lp.setMargins(ArmsUtils.dip2px(ct, 24), ArmsUtils.dip2px(ct, 48), ArmsUtils.dip2px(ct, 24), ArmsUtils.dip2px(ct, 24));
            view.setLayoutParams(lp);
        }
    }

    @Override
    public void setCancelable(boolean flag) {
        super.setCancelable(flag);
        this.view.findViewById(R.id.base_dialog_line2).setVisibility(flag ? View.GONE : View.VISIBLE);
        this.cancel.setVisibility(flag ? View.GONE : View.VISIBLE);
        if (!flag) this.mClickListener = null;
    }

    public void setTitleVisibility(boolean flag) {
        super.setCancelable(flag);
        this.view.findViewById(R.id.base_dialog_line1).setVisibility(flag ? View.GONE : View.VISIBLE);
        this.title.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        cancel = null;
        confirm = null;
        title = null;
        mClickListener = null;
        content = null;
        layout = null;
        view = null;
        ct = null;
    }


    public interface OnShowListener {
        void onShow(Window dialog, View view);
    }

    public interface OnClickListener {
        boolean onConfirm();
    }

    public interface OnConfirmListener {
        boolean onCancel(View view);

        boolean onConfirm(View view);
    }

}
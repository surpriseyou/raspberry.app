package com.upupwe.raspberry.app.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jess.arms.integration.AppManager;
import com.jess.arms.utils.ArmsUtils;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.upupwe.raspberry.R;
import com.upupwe.raspberry.app.common.BaseDialog;
import com.upupwe.raspberry.app.model.DialogModel;
import com.upupwe.raspberry.app.update.APIManager;

public class DialogUtils {

    private static DialogPlus dialog;


    public static void showConfirmDialog(DialogModel model, DialogClickListener clickListener) {
        if (model != null) {
            Context context = AppManager.getAppManager().getCurrentActivity();
            dialog = DialogPlus.newDialog(context)
                    .setHeader(generateHeader(context, model.getTitle()))
                    .setFooter(generateFooter(context, model, clickListener))
                    .setCancelable(false)
                    .setContentHolder(new ViewHolder(generateContent(context, model)))
                    .setGravity(Gravity.CENTER)
                    .setContentBackgroundResource(R.drawable.shape_round_corner)
                    .create();


            dialog.show();
        }
    }

    public static DialogPlus makeDialog(DialogModel model, DialogClickListener clickListener) {
        if (model != null) {
            Context context = AppManager.getAppManager().getCurrentActivity();
            DialogPlus dialog = DialogPlus.newDialog(context)
                    .setHeader(generateHeader(context, model.getTitle()))
                    .setFooter(generateFooter(context, model, clickListener))
                    .setCancelable(false)
                    .setContentHolder(new ViewHolder(generateContent(context, model)))
                    .setGravity(Gravity.CENTER)
                    .setContentBackgroundResource(R.drawable.shape_round_corner)
                    .create();

            return dialog;
        }
        return null;
    }


    public static void showDialog(DialogModel dialogModel) {
//        View dialogView = inflateBase(dialogModel);
        Context context = AppManager.getAppManager().getCurrentActivity();
        TextView textView = new TextView(context);
        textView.setText("title");
        dialog = DialogPlus.newDialog(context)
                .setContentHolder(new ViewHolder(generateContent(context, dialogModel)))
                .setGravity(Gravity.CENTER)
                .setContentBackgroundResource(R.drawable.shape_round_corner)
                .setHeader(generateHeader(context, "title"))
                .setFooter(generateFooter(context, dialogModel, null))
                .setCancelable(false)
                .create();

        dialog.show();
    }


    public static void showWaitting() {

    }

    public static void closeWaitting() {

    }

    private static View generateHeader(Context context, String title) {
        TextView textView = new TextView(context);
        textView.setText(title);
        textView.setTextColor(ArmsUtils.getColor(context, R.color.main_text_color));
        textView.setGravity(Gravity.CENTER);
        textView.setHeight(ArmsUtils.dip2px(context, 50));
        return textView;
    }

    private static View generateFooter(Context context, DialogModel model, DialogClickListener clickListener) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setGravity(Gravity.CENTER);
//        linearLayout.setBackgroundColor(ArmsUtils.getColor(context, R.color.main_color));
        if (model != null) {
            int color = ArmsUtils.getColor(context, R.color.main_text_color);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            if (!TextUtils.isEmpty(model.getConfirom())) {
                Button btnConfirm = new Button(context);
                btnConfirm.setOnClickListener(v -> {
                    if (clickListener != null) {
                        clickListener.onConfirm();
                    }
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                });
                btnConfirm.setTextColor(color);
                btnConfirm.setText(model.getConfirom());
                btnConfirm.setPadding(5, 5, 5, 5);
                btnConfirm.setLayoutParams(layoutParams);
                linearLayout.addView(btnConfirm);
            }

            if (!TextUtils.isEmpty(model.getCancel())) {
                Button btnCancel = new Button(context);
                btnCancel.setText(model.getCancel());
                btnCancel.setOnClickListener(view -> {
                    if (clickListener != null) {
                        clickListener.onCancel();
                    }
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                });

                btnCancel.setTextColor(color);
                btnCancel.setPadding(5, 5, 5, 5);
                btnCancel.setLayoutParams(layoutParams);
                btnCancel.setLeft(ArmsUtils.dip2px(context, 20));
                linearLayout.addView(btnCancel);
            }
        }
        return linearLayout;
    }

    private static View generateContent(Context context, DialogModel model) {
        if (model != null && model.getContentView() != null) {
            return model.getContentView();
        }
        TextView textView = new TextView(context);
        if (model != null) {
            if (!TextUtils.isEmpty(model.getContent())) {
                textView.setTextColor(ArmsUtils.getColor(context, R.color.main_text_color));
                textView.setGravity(Gravity.CENTER);
                textView.setHeight(ArmsUtils.dip2px(context, 50));
                textView.setText(model.getContent());
            }
        }
        return textView;
    }

    private static View inflateBase(DialogModel model) {
        View view = AppManager.getAppManager().getCurrentActivity().getLayoutInflater().inflate(R.layout.dialog_base, null);
        if (model != null) {
            if (!TextUtils.isEmpty(model.getTitle())) {
                TextView textView = view.findViewById(R.id.base_dialog_title_text);
                textView.setText(model.getTitle());
            }
            if (!TextUtils.isEmpty(model.getContent())) {
                TextView textView = view.findViewById(R.id.base_dialog_content);
                textView.setText(model.getContent());
            }
        }
        return view;
    }

    public interface DialogClickListener {
        void onConfirm();

        void onCancel();
    }


    static Dialog mWaitDialog;


    static public void showWaitingDialog() {
        Context context = AppManager.getAppManager().getCurrentActivity();
        if (mWaitDialog == null) mWaitDialog = new Dialog(context);
        if (mWaitDialog.isShowing()) return;
        mWaitDialog = new Dialog(context, R.style.dialog_no_bg);
        ProgressBar progressbar = new ProgressBar(context);
        progressbar.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.animate_waiting));
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.CENTER_IN_PARENT);
        int dp32 = ArmsUtils.dip2px(context, 32);
        lp.width = dp32;
        lp.height = dp32;
        RelativeLayout view = new RelativeLayout(context);
        view.addView(progressbar, lp);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackground(context.getResources().getDrawable(R.drawable.dialog_backgroup));
        }
        int dp90 = ArmsUtils.dip2px(context, 90);
        int dp60 = ArmsUtils.dip2px(context, 60);
        FrameLayout.LayoutParams fllp = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        fllp.width = dp90;
        fllp.height = dp60;
        fllp.gravity = Gravity.CENTER;
        mWaitDialog.addContentView(view, fllp);
        mWaitDialog.setCanceledOnTouchOutside(false);
        try {
            mWaitDialog.show();
        } catch (Exception ignored) {
        }
    }

    static public void closeWaitingDialog() {
        if (mWaitDialog == null) return;
        Activity activity = AppManager.getAppManager().getCurrentActivity();
        if (activity != null && !activity.isFinishing()) {
            try {
                if (mWaitDialog.isShowing())
                    mWaitDialog.cancel();
                mWaitDialog = null;
            } catch (Exception ignored) {
            }
        }
    }

    static public BaseDialog showDialog(int layoutid, String title, String confirmText, String cancelText, int width, int height,
                                        BaseDialog.OnShowListener onShowListener,
                                        BaseDialog.OnConfirmListener confirmListener) {
        Activity activity = AppManager.getAppManager().getCurrentActivity();
        if (activity == null || activity.isFinishing()) {
            return null;
        }
        View view = activity.getLayoutInflater().inflate(layoutid, null);
        BaseDialog dialog = new BaseDialog(activity, view, title, width, height);
        if (onShowListener != null) dialog.setOnShowListener(onShowListener);
        dialog.setCancelable(false);
        if (confirmListener != null) dialog.setOnConfirmListener(confirmListener);
        if (confirmText != null && !confirmText.equals("")) dialog.confirm.setText(confirmText);
        if (cancelText != null && !cancelText.equals("")) dialog.cancel.setText(cancelText);
        dialog.show();
        return dialog;
    }

    //region WarningDialog
    static public BaseDialog WarningDialog(String message) {
        return WarningDialog("", message, "", "", null);
    }

    static public BaseDialog WarningDialog(String title, String message, BaseDialog.OnConfirmListener onConfirmListener) {
        return WarningDialog(title, message, "", "", onConfirmListener);
    }

    static public BaseDialog WarningDialog(String title, String message) {
        return WarningDialog(title, message, "", "", null);
    }

    //endregion
    static public BaseDialog WarningDialog(String title, String message, String cancelText, String confirmText, BaseDialog.OnConfirmListener onConfirmListener) {
        Activity activity = AppManager.getAppManager().getCurrentActivity();
        if (activity == null || activity.isFinishing()) {
            return null;
        }
        if (message == null || message.equals("")) return null;
        if (title.equals("")) title = "警告消息";
        TextView text = new TextView(activity);
        text.setText(message);
        text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        text.setPadding(32, 48, 32, 48);
        text.setMaxWidth(ArmsUtils.dip2px(activity, 400));
        text.setLineSpacing(0, 1.2F);
        text.setTextColor(activity.getResources().getColor(Color.RED));
        BaseDialog dialog = new BaseDialog(activity, text, title, 300, 0);
        dialog.setCancelable(false);
        if (cancelText != null && !cancelText.equals("")) dialog.cancel.setText(cancelText);
        if (confirmText != null && !confirmText.equals("")) dialog.confirm.setText(confirmText);
        if (onConfirmListener != null) dialog.setOnConfirmListener(onConfirmListener);
        dialog.show();
        return dialog;
    }

    //region AlertDialog
    static public void AlertDialog(String message) {
        AlertDialog("", message, "", null);
    }

    static public void AlertDialog(String message, String confirmText, BaseDialog.OnClickListener onClickListener) {
        AlertDialog("", message, confirmText, onClickListener);
    }

    static public void AlertDialog(String title, String message) {
        AlertDialog(title, message, "", null);
    }

    //endregion
    static public void AlertDialog(String title, String message, String confirmText, BaseDialog.OnClickListener onClickListener) {
        Context context = AppManager.getAppManager().getCurrentActivity();
        if (TextUtils.isEmpty(message)) return;
        if (TextUtils.isEmpty(title)) title = "提示消息";
        TextView text = new TextView(context);
        text.setText(message);
        text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        text.setPadding(32, 48, 32, 48);
        text.setMaxWidth(ArmsUtils.dip2px(context, 400));
        text.setLineSpacing(0, 1.2F);
        BaseDialog dialog = new BaseDialog(context, text, title, 300, 0);
        dialog.setCancelable(true);
        if (confirmText != null && confirmText != "") dialog.confirm.setText(confirmText);
        if (onClickListener != null) dialog.setOnClickListener(onClickListener);
        dialog.show();
        if (title == null) dialog.setTitleVisibility(false);
    }

    //region ConfirmDialog
    public static void ConfirmDialog(String message, BaseDialog.OnConfirmListener clickListener) {
        ConfirmDialog(new DialogModel("", message), clickListener);
    }

    public static void ConfirmDialog(String title, String message, BaseDialog.OnConfirmListener clickListener) {
        ConfirmDialog(new DialogModel(title, message), clickListener);
    }
    //endregion

    static public void ConfirmDialog(DialogModel model, BaseDialog.OnConfirmListener confirmListener) {

        Context context = AppManager.getAppManager().getCurrentActivity();
        if (TextUtils.isEmpty(model.getContent())) return;
        TextView text = new TextView(context);
        text.setText(model.getContent());
        text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        text.setPadding(32, 48, 32, 48);
        text.setMaxWidth(ArmsUtils.dip2px(context, 400));
        text.setLineSpacing(0, 1.2F);
        ConfirmDialog(model, text, confirmListener);
    }

    static void ConfirmDialog(DialogModel model, View view, BaseDialog.OnConfirmListener confirmListener) {

        Context context = AppManager.getAppManager().getCurrentActivity();
        BaseDialog dialog = new BaseDialog(context, view, model.getTitle(), 300, 0);
        dialog.setCancelable(false);
        if (confirmListener != null) dialog.setOnConfirmListener(confirmListener);
        if (model.getConfirom() != null && !model.getConfirom().equals(""))
            dialog.confirm.setText(model.getConfirom());
        if (model.getCancel() != null && !model.getCancel().equals(""))
            dialog.cancel.setText(model.getCancel());
        if (model.getTitle() == null) dialog.setTitleVisibility(false);
        if (model.getTitle() != null && model.getTitle().equals("")) dialog.title.setText("确认消息");
        dialog.show();
    }


}

package com.upupwe.raspberry.app.model;

import android.text.TextUtils;
import android.view.View;

public class DialogModel {
    private String title;
    private String content;
    private String confirom;
    private String cancel;
    private View contentView;
    private int width;
    private int height;

    public DialogModel() {
    }

    public DialogModel(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public DialogModel(String title, String content, String confirom, String cancel, View contentView, int width, int height) {
        this.title = title;
        this.content = content;
        this.confirom = confirom;
        this.cancel = cancel;
        this.contentView = contentView;
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public View getContentView() {
        return contentView;
    }

    public void setContentView(View contentView) {
        this.contentView = contentView;
    }

    public String getCancel() {
        if (TextUtils.isEmpty(cancel))
            return "取消";
        return cancel;
    }

    public void setCancel(String cancel) {
        this.cancel = cancel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getConfirom() {
        if (TextUtils.isEmpty(confirom))
            return "确定";
        return confirom;
    }

    public void setConfirom(String confirom) {
        this.confirom = confirom;
    }
}

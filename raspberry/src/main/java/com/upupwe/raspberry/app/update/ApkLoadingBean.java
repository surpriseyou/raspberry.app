package com.upupwe.raspberry.app.update;

public class ApkLoadingBean {
    /**
     * 文件大小
     */
    long total;
    /**
     * 已下载大小
     */
    long progress;

    public long getProgress() {
        return progress;
    }

    public long getTotal() {
        return total;
    }

    public ApkLoadingBean(long total, long progress) {
        this.total = total;
        this.progress = progress;
    }
}
